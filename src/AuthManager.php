<?php

namespace KDA\Laravel\Authentication;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Cache\RateLimiter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Support\Traits\Tappable;
use Illuminate\Validation\ValidationException;
use KDA\Laravel\Authentication\Concerns\CanRedirect;
use KDA\Laravel\Authentication\Concerns\CanRespond;
use KDA\Laravel\Authentication\Concerns\EvaluatesClosure;
use KDA\Laravel\Authentication\Concerns\HandleRequest;
use KDA\Laravel\Authentication\Concerns\HasFlowKey;
use KDA\Laravel\Authentication\Concerns\HasGuard;
use KDA\Laravel\Authentication\Concerns\HasPasswordBroker;
use KDA\Laravel\Authentication\Concerns\HasToken;
use KDA\Laravel\Authentication\Concerns\LoginCredentials;
use KDA\Laravel\Authentication\Concerns\MustVerifyEmail;
use KDA\Laravel\Authentication\Concerns\RegistersUser;
use KDA\Laravel\Authentication\Concerns\SendsResetPassword;
use KDA\Laravel\Authentication\Concerns\ThrottleAuth;
use KDA\Laravel\Authentication\Concerns\ResetsPassword;


//use Illuminate\Support\Facades\Blade;
class AuthManager
{
    use EvaluatesClosure;
    use SendsResetPassword;
    use MustVerifyEmail;
    use ThrottleAuth;
    use HasGuard;
    use CanRespond;
    use CanRedirect;
    use LoginCredentials;
    use HasFlowKey;
    use HandleRequest;
    use RegistersUser;
    use ResetsPassword;
    use HasToken;
    use Conditionable;
    use Tappable;
    protected bool $register_event_listeners = true;
    public function __construct()
    {
        $this->brokerCredentials(function (Request $request) {
            if ($request instanceof Request) {
                return $request->only('email');
            }
            return \Arr::only($request, 'email');
        });

        $this->loginCredentials(function (Request $request) {
            return $request->only($this->getUsernameKey(), 'password');
        });

        $this->broker(function () {
            return Password::broker();
        });

        $this->createUserUsing($this->getDefaultCreateUser());
        $this->validateRegistrationUsing($this->getDefaultValidateRegistration());
        $this->validateSendResetLinkUsing($this->getDefaultvalidateSendResetLink());
        $this->validateResetPasswordUsing($this->getDefaultvalidateResetPassword());
        $this->savePasswordUsing($this->getDefaultSavePassword());
        $this->revokeTokenUsing($this->getDefaultRevokeToken());
        $this->createTokenUsing($this->getDefaultCreateToken());
        $this->registrationResponse($this->getDefaultRegistrationResponse());
    }

    public function attemptLogin(Request $request)
    {
        return $this->getGuard()->attempt(
            $this->getLoginCredentials($request),
            $request->boolean('remember')
        );
    }

    public function validateLogin()
    {
        $request = $this->getRequest();
        return $this->getGuard()->validate(
            $this->getLoginCredentials($request)
        );
    }

    public function authenticate($request): bool
    {
        $this->request($request);
        if ($this->attemptLogin($request)) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
                $request->session()->regenerate();
            }

            $this->clearLoginAttempts();
            return true;
        }
        $this->incrementLoginAttempts();
        return false;
    }


    

    public function changePassword($request): static
    {
        $this->request($request);

        if(!$this->attemptLogin($request)){
           abort(422,__('laravel-authentication::account.cannot_validate_account'));
        }

        $this->savePassword($this->getGuard()->user(),$request->get('new_password'));
       
        return $this;
    }



    public function logout(Request $request): static
    {
        $this->request($request)
        ->revokeToken();
        if(method_exists($this->getGuard(),'logout')){
           $this->getGuard()->logout();
        }
        if ($request->hasSession()) {
            $request->session()->invalidate();
            $request->session()->regenerateToken();
        }
        return $this;
    }

    public function register(Request $request): static
    {
        $this->request($request);
        $this->registerUser();
        //event(new Registered($user = $this->create($data)));
        return $this;
    }

    public function livewireRegister($data):static
    {
        return $this->createRequest($data)
            ->tap(function($manager){
                $user = $manager->createUser();
                $this->triggerAfterCreateUser($user);
                $this->useVerificationCallback();
                $this->emitRegistrationEvent($user);
            })
            ;
        
    }

    public function resetPasswordLink(Request $request): static
    {
        $this->request($request);
        $this->sendResetPasswordLink();
        return $this;
    }

    public function requestResetPassword(Request $request): static
    {
        $this->request($request);
        $this->attemptResetPassword();
        return $this;
    }
    public function verifyEmail(Request $request):static
    {
        $this->request($request)
        ->attemptVerifyEmail();
        return $this;
    }

    public function unregisterEventListeners(){
        $this->register_event_listeners= false;
    }

    public function shouldRegisterEventListeners():bool{
        return $this->register_event_listeners;
    }
}
