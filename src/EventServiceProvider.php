<?php

namespace KDA\Laravel\Authentication;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Support\Facades\Event;
use KDA\Laravel\Authentication\Facades\AuthManager;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        /*   Registered::class => [
            SendEmailVerificationNotification::class,
        ],*/];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        if (AuthManager::shouldRegisterEventListeners()) {
            Event::listen(Registered::class, SendEmailVerificationNotification::class);
        }
    }
}
