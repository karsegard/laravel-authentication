<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Http\Request;

trait LoginCredentials
{
    protected Closure $login_credentials;
    protected Closure | string $username_key = 'email';
    
    public function loginCredentials(Closure $credentials):static
    {
        $this->login_credentials = $credentials;
        return $this;
    }
   
    protected function getLoginCredentials(Request $request)
    {
        return $this->evaluate($this->login_credentials,$this->getEvaluationParameters());
    }

    public function usernameKey(Closure | string $key):static
    {
        $this->username_key = $key;
        return $this;
    }

    public function getUsernameKey()
    {
        return $this->evaluate($this->username_key,$this->getEvaluationParameters());
    }


}
