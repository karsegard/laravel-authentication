<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Auth\Passwords\PasswordBroker;

trait HasPasswordBroker
{
    protected Closure|PasswordBroker $broker;

    protected Closure $broker_crendentials;

    protected ?string $broker_response=null;


    public function brokerResponse($response):static
    {
        $this->broker_response =$response;
        return $this;
    }

    public function clearBrokerResponse():static
    {
        $this->brokerResponse(null);
        return $this;
    }

    public function getBrokerResponse():?string
    {
        return $this->broker_response;   
    }
    

    public function brokerCredentials(Closure $callback): static
    {
        $this->broker_crendentials = $callback;

        return $this;
    }

    public function getBrokerCredentials(): array
    {
        return $this->evaluate($this->broker_crendentials, $this->getEvaluationParameters());
    }

    public function getBroker()
    {
        return $this->evaluate($this->broker);
    }

    public function broker(Closure $broker): static
    {
        $this->broker = $broker;

        return $this;
    }
}
