<?php

namespace KDA\Laravel\Authentication\Concerns;
use Illuminate\Http\Request;
use Illuminate\Cache\RateLimiter;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

use Closure;

trait ThrottleAuth
{
    protected Closure|int $maxAttempts = 5;

    protected Closure|int $decayMinutes = 1;

    public function getDecayMinutes()
    {
        return $this->evaluate($this->decayMinutes);
    }

    public function getMaxAttempts()
    {
        return $this->evaluate($this->maxAttempts);
    }

    protected function getLimiter()
    {
        return app(RateLimiter::class);
    }

    protected function hasTooManyLoginAttempts()
    {
        return $this->getLimiter()->tooManyAttempts(
            $this->getThrottleKey(), $this->getMaxAttempts()
        );
    }

    protected function fireLockoutEvent()
    {
        $request = $this->getRequest();
        event(new Lockout($request));
    }

    protected function throwLockoutResponse()
    {
        $seconds = $this->getLimiter()->availableIn(
            $this->getThrottleKey()
        );

        throw ValidationException::withMessages([
            $this->getUsernameKey() => [trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ])],
        ])->status(Response::HTTP_TOO_MANY_REQUESTS);
    }

    public function ensureAuthenticationThrottle(): static
    {
        if (
            $this->hasTooManyLoginAttempts()) {
            $this->fireLockoutEvent();

            return $this->throwLockoutResponse();
        }

        return $this;
    }

    protected function incrementLoginAttempts()
    {
        $request = $this->getRequest();
        $this->getLimiter()->hit(
            $this->getThrottleKey($request), $this->getDecayMinutes() * 60
        );
    }

    public function getThrottleKey():string
    {
        $request = $this->getRequest();
        return Str::transliterate(Str::lower($request->input($this->getUsernameKey())).'|'.$request->ip());
    }

    protected function clearLoginAttempts()
    {   
        $request = $this->getRequest();
        $this->getLimiter()->clear($this->getThrottleKey($request));
    }

}
