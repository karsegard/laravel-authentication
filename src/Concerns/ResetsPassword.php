<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

trait ResetsPassword
{
    use HasPasswordBroker;

    protected Closure | null $validate_reset_password_callback= null;
    protected Closure | bool $hash_password = false;
    protected Closure | null $save_password_using;

    public function attemptResetPassword():static
    {
        $this->validateResetPassword();
        if(!blank($this->getBrokerResponse())){
            throw  new \Exception("already used broker link. Clear the response before sending a new one or make a new request");
        }
       /* $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );
*/  
        $request = $this->getRequest();
        $response = $this->getBroker()->reset(
            $request->only('email','password','password_confirmation','token'),
            function($user,$password){
                $this->resetPassword($user, $password);
            }
        );
        $this->brokerResponse($response);
        return $this;
    }

    public function hashPassword(Closure | bool $hash):static
    {
        $this->hash_password = $hash;
        return $this;
    }

    public function shouldHashPassword():bool{
        return $this->evaluate($this->hash_password,$this->getEvaluationParameters());
    }

    public function getDefaultSavePassword():Closure
    {
        return function ($user,$password,$manager){
            $user->password = $this->shouldHashPassword() ? Hash::make($password): $password;

            $user->setRememberToken(\Str::random(60));
    
            $user->save();
        };
    }

    protected function savePassword($user,$password){
        return $this->evaluate(
            $this->save_password_using,
            $this->getEvaluationParameters()
                ->put('user',$user)
                ->put('password',$password)
        );
    }

    public function savePasswordUsing(Closure $callback):static
    {
        $this->save_password_using = $callback;
        return $this;
    }

    protected function resetPassword($user,$password):static
    {
        $this->savePassword($user,$password);
        event(new PasswordReset($user));

        return $this;
    }

    public function validateResetPassword():static
    {
        $validation = $this->validate_reset_password_callback;
        if($validation){
            $this->evaluate($validation,$this->getEvaluationParameters());
        }
        return $this;
    }

    public function validateResetPasswordUsing(Closure $closure):static
    {
        $this->validate_reset_password_callback = $closure;
        return $this;
    }

    public function getDefaultvalidateResetPassword():Closure
    {
        return function ($request){
            return $request->validate([
                'token' => 'required',
                'email' => 'required|email',
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);
        };
    }

    public function getResetPasswordResponse()
    {
        $response = $this->getBrokerResponse();
        return $response == $this->getBroker()::PASSWORD_RESET
                    ? $this->getResetPasswordSuccessResponse()
                    : $this->getResetPasswordFailedResponse();
    }


    public function getResetPasswordSuccessResponse()
    {
        $response = $this->getBrokerResponse();
        $request = $this->getRequest();
        return $request->wantsJson()
                    ? new JsonResponse(['message' => trans($response)], 200)
                    : back()->with('status', trans($response));
    }

    protected function getResetPasswordFailedResponse()
    {
        $response = $this->getBrokerResponse();
        $request = $this->getRequest();

        if ($request->wantsJson()) {
            throw ValidationException::withMessages([
                'email' => [trans($response)],
            ]);
        }

        return back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
    }
}
