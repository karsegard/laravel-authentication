<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
trait CanRespond
{
    
  
    public function getAuthenticationSuccessResponse(){
        $request = $this->getRequest();
        return $request->wantsJson()
        ? new JsonResponse([], 204)
        : redirect()->intended($this->getRedirectPath());
    }

    public function getAuthenticationFailureResponse(){
        $request = $this->getRequest();

        throw ValidationException::withMessages([
            $this->getUsernameKey() => [trans('auth.failed')],
        ]);
    }

}
