<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Notifications\ResetPassword;

trait SendsResetPassword
{
    use HasPasswordBroker;

    protected Closure | null $validate_send_reset_link_callback= null;


    public function createResetPasswordUrlUsing($callable):static 
    {
        $this->validate_send_reset_link_callback = function () use ($callable){
           return  ResetPassword::createUrlUsing($callable);
        };
        return $this;
    }

    public function useResetPasswordUrlGenerationCallback()
    {
        $this->evaluate($this->validate_send_reset_link_callback);
    }

    public function sendResetPasswordLink():static{
        $request = $this->getRequest();
        $this->validateSendResetLink();
        if(!blank($this->getBrokerResponse())){
            throw \Exception("already used broker link. Clear the response before sending a new one or make a new request");
        }
      //  dd($this->getBroker());
        $response = $this->getBroker()->sendResetLink(
            $this->getBrokerCredentials($request)
        );
        $this->brokerResponse($response);
        return $this;
    }

    public function validateSendResetLink():static
    {
        $validation = $this->validate_send_reset_link_callback;
        if($validation){
            $this->evaluate($validation,$this->getEvaluationParameters());
        }
        return $this;
    }

    public function validateSendResetLinkUsing(Closure $closure):static
    {
        $this->validate_send_reset_link_callback = $closure;
        return $this;
    }

    public function getDefaultvalidateSendResetLink():Closure
    {
        return function ($request){
            return $request->validate(['email' => 'required|email']);
        };
    }

    public function getSendResetLinkResponse()
    {
        $response = $this->getBrokerResponse();
        return $response == $this->getBroker()::RESET_LINK_SENT
                    ? $this->getSendResetLinkSuccessResponse()
                    : $this->getSendResetLinkFailedResponse();
    }


    public function getSendResetLinkSuccessResponse()
    {
        $response = $this->getBrokerResponse();
        $request = $this->getRequest();
        return $request->wantsJson()
                    ? new JsonResponse(['message' => trans($response)], 200)
                    : back()->with('status', trans($response));
    }

    protected function getSendResetLinkFailedResponse()
    {
        $response = $this->getBrokerResponse();
        $request = $this->getRequest();

        if ($request->wantsJson()) {
            throw ValidationException::withMessages([
                'email' => [trans($response)],
            ]);
        }

        return back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
    }
}
