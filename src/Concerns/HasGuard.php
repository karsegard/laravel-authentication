<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Auth\TokenGuard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

trait HasGuard
{
    protected Closure| Guard|string $guard = 'web';

    public function guard(Closure|Guard|string $guard): static
    {
        $this->guard = $guard;

        return $this;
    }

    public function getGuard():  Guard
    {
        $guard = $this->evaluate($this->guard,$this->getEvaluationParameters());
        if (is_string($guard)) {
            $guard = Auth::guard($guard);
        }

        return $guard;
    }
}
