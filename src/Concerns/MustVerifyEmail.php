<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;

trait MustVerifyEmail
{

    protected Closure | string  $verify_email_route = 'verification.verify';
    protected Closure | int | null $verification_expire = null;

    public function verifyEmailRoute(Closure | string $route): static
    {
        $this->verify_email_route = $route;
        return $this;
    }

    public function getVerifyEmailRoute(): string
    {
        return $this->evaluate($this->verify_email_route, $this->getEvaluationParameters());
    }

    public function getVerifyExpire(): ?int
    {
        return $this->evaluate($this->verification_expire, $this->getEvaluationParameters());
    }

    public function verifyExpire(Closure | int | null $value): static
    {
        $this->verification_expire = $value;
        return $this;
    }

    public function sendVerificationNotification(): static
    {
        $request = $this->getRequest();
        $this->useVerificationCallback();
        $user = $request->user();
        $user->sendEmailVerificationNotification();
        return $this;
    }


    public function getDefaultValidationUrlGenerator(): Closure
    {

        return function ($notifiable) {
            $expires = $this->getVerifyExpire() ?? config('auth.verification.expire', 60);
            $route = $this->getVerifyEmailRoute();
            return URL::temporarySignedRoute(
                $route,
                Carbon::now()->addMinutes($expires),
                [
                    'id' => $notifiable->getKey(),
                    'hash' => sha1($notifiable->getEmailForVerification()),
                ]
            );
        };
    }

    public function attemptVerifyEmail()
    {
        $request = $this->getRequest();
        if (!hash_equals((string) $request->get('id'), (string) $request->user()->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string) $request->get('hash'), sha1($request->user()->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }
    }

    public function getVerifiedResponse()
    {
        $request = $this->getRequest();
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect($this->getRedirectPath())->with('verified', true);
    }
}
