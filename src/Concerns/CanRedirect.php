<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
trait CanRedirect
{
    protected Closure|string $redirectPath='/login';
    
    public function getRedirectPath()
    {
        return $this->evaluate($this->redirectPath,['key'=>$this->getFlowKey()]);
    }
}
