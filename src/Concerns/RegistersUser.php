<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\JsonResponse;

trait RegistersUser
{
    protected Closure $create_user;

    protected Closure | string $registration_event=Registered::class;

    protected Closure | null $register_validation= null;

    protected Closure | null $after_creating_user = null;

    protected $registered_user = null;

    protected Closure $registration_response;

    protected ?Closure $verification_url_callback=null;
    //protected ?Closure $original_verification_url_callback=null;


    public function createVerificationUrlUsing(Closure $callable):static
    {
        $this->verification_url_callback = function () use ($callable){
            return VerifyEmail::createUrlUsing($callable);
        };
        return $this;
    }
  /*  public function backupVerificationCallback():static
    {
        $this->original_verification_url_callback=VerifyEmail::$createUrlCallback;
        return $this;
    }

    public function restoreVerificationCallback():static 
    {
        VerifyEmail::createUrlUsing($this->original_verification_url_callback);
        return $this;
    }*/

    public function useVerificationCallback():static
    {
        $this->evaluate($this->verification_url_callback);
        return $this;
    }

    public function getDefaultValidateRegistration():Closure{
        return function($request){
            return $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        };
    }

    public function getDefaultCreateUser():Closure{
       return function($request) :Model{
            return \App\Models\User::create($request->all());
        };
    }


    public function createUserUsing(Closure $callback):static
    {
        $this->create_user = $callback;
        return $this;
    }


    public function registeredUser($user):static
    {
        $this->registered_user = $user;
        return $this;
    }
    public function getRegisteredUser()
    {
        return $this->registered_user;
    }

    public function createUser():Model
    {
        $user= $this->evaluate($this->create_user,$this->getEvaluationParameters());
        $this->registeredUser($user);
        return $user;
    }

    public function emitRegistrationEvent($user):static
    {
        $class= $this->evaluate($this->registration_event,$this->getEvaluationParameters());
        event(new $class($user));
        return $this;
    }


    public function validateRegistrationUsing(Closure $callback):static
    {
        $this->register_validation=$callback;
        return $this;
    }

    public function validateRegistration():static{
        $validation = $this->register_validation;
        if($validation){
            $this->evaluate($validation,$this->getEvaluationParameters());
        }
        return $this;
    }

    protected function triggerAfterCreateUser($user):static
    {   
        $this->evaluate(
            $this->after_creating_user,
            $this->getEvaluationParameters()->put('user',$user)
        );
        return $this;
    }

    public function afterCreatingUser(Closure | null $callback):static{
        $this->after_creating_user = $callback;
        return $this;
    }

    public function registerUser():static
    {
        $this->validateRegistration();
        $user = $this->createUser();
        $this->triggerAfterCreateUser($user);
        $this->useVerificationCallback();
        $this->emitRegistrationEvent($user);
      //  $this->getGuard()->login($user);
        
        return $this;
    }

    public function sendVerificationEmail():static
    {
        $request = $this->getRequest();
        if($user = $request->user()){
            $user->sendEmailVerificationNotification();
        }
        return $this;
    }


    public function getDefaultRegistrationResponse(){
        return function($user,$request){
            return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->getRedirectPath());
        };
    }

    public function registrationResponse(Closure $callback):static
    {
        $this->registration_response = $callback;
        return $this;
    }

    public function getRegisterResponse(){
        return $this->evaluate($this->registration_response,$this->getEvaluationParameters()->put('user',$this->getRegisteredUser()));
    }

    public function autologin()
    {
        return $this->tap(function ($manager) {
            $manager->getGuard()->login($manager->getRegisteredUser());
        });
    }
}
