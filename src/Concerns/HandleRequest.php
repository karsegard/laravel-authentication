<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

trait HandleRequest
{
    protected Closure | Request | null $request=null;
    

    public function request(Closure | Request | null $request):static
    {
        $this->request = $request;
        return $this;
    }

    public function getRequest(): ?Request{
        return $this->evaluate($this->request);
    }

    public function createRequest(array $data):static{
        $request = Request::create('forged_request', 'POST',$data);
        $this->request($request);
        return $this;
    }
}
