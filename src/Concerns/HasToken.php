<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Auth;

trait HasToken
{
    protected Closure|bool $user_model_supports_token = false;
    protected Closure | null $revoke_token_using = null;
    protected Closure | null $create_token_using = null;
    protected  $token = null;
    public function useToken(Closure| bool $bool): static
    {
        $this->user_model_supports_token = $bool;

        return $this;
    }

    public function hasTokenSupport(): bool
    {
        $token = $this->evaluate($this->user_model_supports_token,$this->getEvaluationParameters());
       
        return $token;
    }

    public function revokeTokenUsing(Closure | null $callback):static
    {
        $this->revoke_token_using= $callback;
        return $this;
    }

    public function revokeToken():static
    {
        if($this->hasTokenSupport()){
           $this->evaluate($this->revoke_token_using,$this->getEvaluationParameters());
        }
        return $this;
    }

    public function getDefaultRevokeToken(){
        return function(){
            if($this->getGuard()->check()){
                $this->getGuard()->user()->token()->revoke();
            }
        };
    }

    public function createTokenUsing(Closure | null $callback):static
    {
        $this->create_token_using= $callback;
        return $this;
    }

    public function createToken($name,$unique=true):static
    {
       // dump($this->getGuard()->user()->tokens);
        //$existing_token = $this->getGuard()->user()->tokens->where('revoked',false)->where('name',$name)->first();
        if($this->hasTokenSupport()){
           $token = $this->evaluate($this->create_token_using,$this->getEvaluationParameters()->put('name',$name));
           $this->token = $token;
        }
        return $this;
    }

    public function getDefaultCreateToken(){
        return function($name){
            return $this->getGuard()->user()->createToken($name);
        };
    }

    
    public function getToken(){
        return $this->token->accessToken;
    }
}
