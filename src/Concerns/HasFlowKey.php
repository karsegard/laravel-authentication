<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;

trait HasFlowKey
{
    protected Closure|string $flow_key = 'web';

    public function flowKey(Closure|string $flow_key): static
    {
        $this->flow_key = $flow_key;

        return $this;
    }

    public function getFlowKey(): string
    {
        $key = $this->evaluate($this->flow_key);

        return $key;
    }
}
