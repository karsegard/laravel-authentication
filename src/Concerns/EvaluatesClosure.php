<?php

namespace KDA\Laravel\Authentication\Concerns;

use Closure;
use Illuminate\Support\Collection;
trait EvaluatesClosure
{
    public function evaluate($value, array | Collection $parameters = [])
    {
        if($parameters instanceof Collection){
            $parameters = $parameters->all();
        }
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }

    public function getEvaluationParameters(array $exceptKeys=[]):Collection
    {
        $args =  collect([
           
        ])->when(!isset($exceptKeys['flow_key']),function($collection){
            return $collection->put('flow_key',$this->getFlowKey());
        })->when(!isset($exceptKeys['request']),function($collection){
            return $collection->put('request',$this->getRequest());
        })->when(!isset($exceptKeys['manager']),function($collection){
            return $collection->put('manager',$this);
        });
        return $args;
    }
    
}
