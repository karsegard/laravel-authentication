<?php

namespace KDA\Laravel\Authentication;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Support\Facades\Event;
use KDA\Laravel\Authentication\AuthManager as Library;
use KDA\Laravel\Authentication\Facades\AuthManager as Facade;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasProviders;
use KDA\Laravel\Traits\HasTranslations;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasProviders;
    use HasTranslations;

    protected $packageName = 'laravel-authentication';
    protected $additionnalProviders=[
        EventServiceProvider::class,
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

     // trait \KDA\Laravel\Traits\HasConfig;
     //    registers config file as
     //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/authentication.php' => 'kda.authentication',
    ];

    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
     
    }
}
