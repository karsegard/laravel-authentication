<?php

namespace KDA\Laravel\Authentication\Facades;

use Illuminate\Support\Facades\Facade;

class AuthManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
