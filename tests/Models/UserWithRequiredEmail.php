<?php

namespace KDA\Tests\Models;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail as AuthMustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @mixin IdeHelperUser
 */
class UserWithRequiredEmail extends User implements AuthMustVerifyEmail
{
   use MustVerifyEmail;
   protected $table="users";
}
