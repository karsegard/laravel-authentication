<?php

namespace KDA\Tests;

use Illuminate\Foundation\Testing\RefreshDatabaseState;
use KDA\Laravel\Authentication\ServiceProvider as ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;
use Illuminate\Http\Request;
use Illuminate\Testing\TestResponse;
use Illuminate\Routing\Pipeline;
use KDA\Tests\Models\User;

class TestCase extends ParentTestCase
{

    public function createUser(){
        return  User::factory()->create(['password'=>'password']);;
      }
  
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [

            ServiceProvider::class,
        ];
    }

    protected function runPackageMigrations($app)
    {
        $provider = $this->getPackageProviders($app)[0];
        $traits = class_uses_recursive($provider);

        if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
            $this->artisan('migrate', ['--database' => 'mysql'])->run();
            $this->beforeApplicationDestroyed(function () {
                $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
                RefreshDatabaseState::$migrated = false;
            });
        }
    }

    protected function callAfterResolving($name, $callback)
    {
        $this->app->afterResolving($name, $callback);

        if ($this->app->resolved($name)) {
            $callback($this->app->make($name), $this->app);
        }
    }

    protected function loadTestMigrations($paths)
    {
        $this->callAfterResolving('migrator', function ($migrator) use ($paths) {
            foreach ((array) $paths as $path) {
                $migrator->path($path);
            }
        });
    }

    protected function getEnvironmentSetUp($app)
    {
        $this->app = $app;
    //'default' => env('QUEUE_CONNECTION', 'sync'),
        $app['config']->set('auth.providers.users.model', User::class);
        $app['config']->set('queue.default','sync');
        $this->loadTestMigrations([__DIR__.'/database/migrations']);
        $this->runPackageMigrations($app);
    }

    /**
     * Handle Request using the following pipeline.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  callable  $callback
     * @return \Illuminate\Testing\TestResponse
     */
    protected function handleRequestUsing(Request $request, callable $callback)
    {
        return new TestResponse(
            (new Pipeline($this->app))
                ->send($request)
                ->through([
                    \Illuminate\Session\Middleware\StartSession::class,
                ])
                ->then($callback)
        );
    }
}
