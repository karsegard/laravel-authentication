<?php

namespace KDA\Tests\Unit;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Authentication\Facades\AuthManager;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;

class LoginTest extends TestCase
{
  use RefreshDatabase;





    public function authControllerLogin($request){
      if(AuthManager::authenticate($request)){
          return AuthManager::getAuthenticationSuccessResponse($request);
      }else{
        return AuthManager::getAuthenticationFailureResponse($request);
      }
    }

    /** @test */
    public function it_can_authenticate_a_user()
    {
        Event::fake();

        $user = $this->createUser();

        $request = Request::create('/login', 'POST', [
            'email' => $user->email,
            'password' => 'password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
           return $this->authControllerLogin($request);
        });
        $response->assertStatus(204);
        Event::assertDispatched(function (Attempting $event) {
            return $event->remember === false;
        });
    }


    /** @test */
    public function it_can_authenticate_a_user_with_remember_as_false()
    {
        Event::fake();

        $user = $this->createUser();


        $request = Request::create('/login', 'POST', [
            'email' => $user->email,
            'password' => 'password',
            'remember' => false,
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
          return $this->authControllerLogin($request);

        })->assertStatus(204);

        Event::assertDispatched(function (Attempting $event) {
            return $event->remember === false;
        });
    }


   /** @test */
    public function it_can_authenticate_a_user_with_remember_as_true()
    {
        Event::fake();

        $user = $this->createUser();

        $request = Request::create('/login', 'POST', [
            'email' => $user->email,
            'password' => 'password',
            'remember' => true,
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
          return $this->authControllerLogin($request);

        })->assertStatus(204);

        Event::assertDispatched(function (Attempting $event) {
            return $event->remember === true;
        });
    }

     /** @test */
     public function it_cant_authenticate_a_user_with_invalid_password()
     {
         $user = $this->createUser();
 
         $request = Request::create('/login', 'POST', [
             'email' => $user->email,
             'password' => 'invalid-password',
         ], [], [], [
             'HTTP_ACCEPT' => 'application/json',
         ]);
 
         $response = $this->handleRequestUsing($request, function ($request) {
             return $this->authControllerLogin($request);
         });

        // dump($response);
         $response->assertUnprocessable();
 
         $this->assertInstanceOf(ValidationException::class, $response->exception);
         $this->assertSame([
             'email' => [
                 'These credentials do not match our records.',
             ],
         ], $response->exception->errors());
     }
     public function it_cant_authenticate_unknown_credential()
     {
         $request = Request::create('/login', 'POST', [
             'email' => 'taylor@laravel.com',
             'password' => 'password',
         ], [], [], [
             'HTTP_ACCEPT' => 'application/json',
         ]);
 
         $response = $this->handleRequestUsing($request, function ($request) {
             return $this->authControllerLogin($request);
         })->assertUnprocessable();
 
         $this->assertInstanceOf(ValidationException::class, $response->exception);
         $this->assertSame([
             'email' => [
                 'These credentials do not match our records.',
             ],
         ], $response->exception->errors());
     }


     
}