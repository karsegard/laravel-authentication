<?php

namespace KDA\Tests\Unit;

use Carbon\Carbon;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Authentication\Facades\AuthManager;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Illuminate\Testing\TestResponse;
use Illuminate\Routing\Pipeline;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use KDA\Laravel\Authentication\AuthManager as AuthenticationAuthManager;
use KDA\Tests\Models\UserWithRequiredEmail;

class RegisterTest extends TestCase
{
    use RefreshDatabase;


    public function authControllerRegister($request)
    {
        AuthManager::createUserUsing(function ($request) {
            return User::create($request->all());
        })
        ->validateRegistrationUsing(function ($request, $flow_key) {
            return Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ])->validate();
        })
        ->afterCreatingUser(function($user){
            $user->email_verified_at = Carbon::now();
            $user->save();
        })
        ;
        AuthManager::register($request);
        return AuthManager::getRegisterResponse();
    }


    /** @test */
    public function it_can_register_a_user()
    {
        Notification::fake();
        Event::fake();
        $request = Request::create('/register', 'POST', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
            'password' => 'secret-password',
            'password_confirmation' => 'secret-password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->authControllerRegister($request);
        });
        //dump($response);
        $response->assertCreated();
        $user = User::first();
        $this->assertNotNull($user->email_verified_at); // testing after create user;
        Event::assertDispatched(Registered::class, function (Registered $event) {
            return $event->user instanceof User;
        });
        $this->assertDatabaseHas('users', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
        ]);
    }



    public function authControllerVerifiedRegister($request)
    {
        AuthManager::createUserUsing(function ($request) {
            return UserWithRequiredEmail::create($request->all());
        })
            ->createVerificationUrlUsing(function ($user) {
                return 'https://whatever.com/' . $user->getKey() . "/" . sha1($user->getEmailForVerification());
            })
            ->validateRegistrationUsing(function ($request, $flow_key) {
                return Validator::make($request->all(), [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                ])->validate();
            });
        AuthManager::register($request);
        return AuthManager::getRegisterResponse();
    }


    /** @test */
    public function it_can_register_a_user_with_email_verification_send_event()
    {

        $request = Request::create('/register', 'POST', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
            'password' => 'secret-password',
            'password_confirmation' => 'secret-password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        Event::fake();

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->authControllerVerifiedRegister($request);
        });

        $response->assertCreated();

        $this->assertDatabaseHas('users', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
        ]);
        $user = UserWithRequiredEmail::first();
        Event::assertDispatched(Registered::class, function (Registered $event) {
            $this->assertTrue($event->user instanceof UserWithRequiredEmail);
            return true;
        });
    }
    /** @test */
    public function it_can_register_a_user_with_email_verification()
    {

        $request = Request::create('/register', 'POST', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
            'password' => 'secret-password',
            'password_confirmation' => 'secret-password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        Notification::fake();

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->authControllerVerifiedRegister($request);
        });

        $response->assertCreated();

        $this->assertDatabaseHas('users', [
            'name' => 'Taylor Otwell',
            'email' => 'taylor@laravel.com',
        ]);
        $user = UserWithRequiredEmail::first();

        Notification::assertSentTo(
            $user,
            VerifyEmail::class,
            function ($notification, $channels) use ($user) {
                $mailData = $notification->toMail($user);
              dump($mailData);
                $this->assertStringContainsString('https://whatever.com', $mailData->actionUrl);
                /* $this->assertStringContainsString("Start date: {$subscriptionData['start_date']}", $mailData->render());
         */
                return true;
            }
        );
    }
}
