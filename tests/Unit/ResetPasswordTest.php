<?php

namespace KDA\Tests\Unit;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Authentication\Facades\AuthManager;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Notification;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;




    public function createUser()
    {
        return  User::factory()->create(['password' => 'password']);;
    }


    public function resetController($request)
    {
        return AuthManager::createResetPasswordUrlUsing(function ($user, string $token) {
            return 'https://myfrontend.com/reset?token=' . $token;
        })
        ->resetPasswordLink($request)
        ->getSendResetLinkResponse();
    }

    /** @test */
    public function it_can_request_reset_password_link_user()
    {
        Event::fake();
        Mail::fake();
        Notification::fake();

        $user = $this->createUser();
        Route::get('/reset', function () {
        })->name('password.reset');

        $request = Request::create('/login/reset', 'POST', [
            'email' => $user->email,
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->resetController($request);
        });
        $response->assertStatus(200);

        //   $repository = $this->app['auth.password']->broker()->getRepository();
        //  dump($repository);
        Notification::assertSentTo(
            [$user],
            ResetPassword::class,
            function ($notification, $channels) use ($user) {
                $mailData = $notification->toMail($user);
                //   dump($mailData);
                $this->assertStringContainsString('https://myfrontend.com', $mailData->actionUrl);
                /* $this->assertStringContainsString("Start date: {$subscriptionData['start_date']}", $mailData->render());
             */
                return true;
            }
        );
        //  Mail::assertSent(ResetPassword::class);
        // Event::assertDispatched(function (PasswordReset $event) {
        ///return $event->remember === false;
        //    });
    }

    /** @test */
    public function it_can_reset_password()
    {
        Event::fake();
        Notification::fake();

        $user = $this->createUser();
        Route::get('/reset', function () {
        })->name('password.reset');

        $request = Request::create('/login/reset', 'POST', [
            'email' => $user->email,
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
            return AuthManager::createResetPasswordUrlUsing(function ($user, string $token) {
                return $token;
            })
            ->resetPasswordLink($request)
            ->getSendResetLinkResponse();
        });
        $response->assertStatus(200);

        //   $repository = $this->app['auth.password']->broker()->getRepository();
        //  dump($repository);
        Notification::assertSentTo(
            [$user],
            ResetPassword::class,
            function ($notification, $channels) use ($user) {
                $mailData = $notification->toMail($user);
                //   dump($mailData);
              //  $this->assertStringContainsString('https://myfrontend.com', $mailData->actionUrl);
                /* $this->assertStringContainsString("Start date: {$subscriptionData['start_date']}", $mailData->render());
             */
              //  dump( $mailData->actionUrl);

                    $result = AuthManager::getGuard()->attempt(['email'=>$user->email,'password'=>'password']);
                    $this->assertTrue($result);
                    $request = Request::create('/login/reset', 'POST', [
                        'email' => $user->email,
                        'token'=>$mailData->actionUrl,
                        'password'=>'new_password',
                        'password_confirmation'=>'new_password'
                    ], [], [], [
                        'HTTP_ACCEPT' => 'application/json',
                    ]);
                    AuthManager::clearBrokerResponse()->requestResetPassword($request);
                    $result = AuthManager::getGuard()->attempt(['email'=>$user->email,'password'=>'new_password']);
                    $this->assertTrue($result);
                    return $result;
            }
        );
        //  Mail::assertSent(ResetPassword::class);
        // Event::assertDispatched(function (PasswordReset $event) {
        ///return $event->remember === false;
        //    });
    }


    /** @test */
    public function it_cant_request_reset_password_link_inexisting_user()
    {
        Event::fake();
        Mail::fake();
        Notification::fake();

        $user = $this->createUser();
        Route::get('/reset', function () {
        })->name('password.reset');

        $request = Request::create('/login/reset', 'POST', [
            'email' => 'babybel@test.com',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);

        $response = $this->handleRequestUsing($request, function ($request) {
            return $this->resetController($request);
        });
        $response->assertStatus(422);
        Notification::assertNothingSent();
        //  Mail::assertSent(ResetPassword::class);
        /* Event::assertDispatched(function (Attempting $event) {
           return $event->remember === false;
       });*/
    }
}
