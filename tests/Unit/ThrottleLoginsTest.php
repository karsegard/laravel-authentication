<?php

namespace KDA\Tests\Unit;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Authentication\Facades\AuthManager;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Notification;
use KDA\Laravel\Authentication\Concerns\ThrottleAuth;

class ThrottleLoginsTest extends TestCase
{
    use RefreshDatabase;


    public function emailProvider(): array
    {
        return [
            'lowercase special characters' => ['ⓣⓔⓢⓣ@ⓛⓐⓡⓐⓥⓔⓛ.ⓒⓞⓜ', 'test@laravel.com'],
            'uppercase special characters' => ['ⓉⒺⓈⓉ@ⓁⒶⓇⒶⓋⒺⓁ.ⒸⓄⓂ', 'test@laravel.com'],
            'special character numbers' =>['test⑩⓸③@laravel.com', 'test1043@laravel.com'],
            'default email' => ['test@laravel.com', 'test@laravel.com'],
        ];
    }
    /**
     * @test
     * @dataProvider emailProvider
     */
    public function it_can_generate_throttle_key($email,$expectedEmail)
    {
        $request = Request::create('/login', 'POST', [
            'email' => $email,
            'password' => 'password',
        ], [], [], [
            'HTTP_ACCEPT' => 'application/json',
        ]);
        $key = AuthManager::request($request)->getThrottleKey();
 
        $this->assertSame($expectedEmail . '|127.0.0.1',  $key);
    }

    
}
