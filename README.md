# This is my package laravel-authentication

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-authentication.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-authentication)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-authentication.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-authentication)

## About

This is a headless & controllerless rewrite of laravel/ui. The main goal is to keep it simpler than laravel/fortify.

It is based around one Facade `AuthManager`

## Installation

```bash
composer require kda/laravel-authentication
```

### Update your `Authenticatable` models

if you need to use Email Verification you can modify your model  by adding the following contract  and trait

```php
<?php

namespace App\Models;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable implements MustVerifyEmailContract
{
   use MustVerifyEmail;
 
}
```

### Routes

Since it's controller less, it's your job to create your controllers and route. You can use the ones provided by this package: kda/laravel-authentication-controllers.
By default some are mandatory, like the `password.reset` route which is used by the laravel password broker by default or the `verification.verify` route.

You can override this by using one of the `createResetPasswordUrlUsing` method

```php
    AuthManager::createResetPasswordUrlUsing(function ($user, string $token) {
        return 'https://myfrontend.com/reset?token=' . $token;
    })
```

Routes checklist:

* [ ] authenticate
* [ ] register
* [ ] request reset password link
* [ ] reset password `password.reset`
* [ ] verify email `verification.verify`


Routes views checklist:

* [ ] Login form
* [ ] Register form
* [ ] Request Reset password form
* [ ] Reset password form
* [ ] Change password form


### Usage

Basically, we have 4 main features

Registration  `AuthManager::register()`
Authentication `AuthManager::authenticate()`
Reset passwords `AuthManager::resetPassword()`
Change password `AuthManager::changePassword()`

All main feature needs to call `request($request)` before being called.

### Advanced usage

#### Flow key

If you want to manage multiple auth and registration types you can use the flow key to customize the lib behavior.
_This is an example, and will not work as is, You still have to configure your auth in the config files._
for example:

```php
    // in a provider

    AuthManager::createUserUsing(function($flow_key,$request){
        return match($flow_key){
            'teacher'=> Teacher::create($request->all()),
            'student'=> Student::create($request->all()),
        };
    });

    AuthManager::afterCreatingUser(function($flow_key,$request,$user){
        return match($flow_key){
            'teacher'=> $user->fill(['role'=>'teacher'])->save(),
            'student'=> $user->fill(['role'=>'student'])->save(),
        };
    });

    AuthManager::guard(function($flow_key){
        return match($flow_key){
            'teacher'=> 'web',
            'student'=> 'api'
        }
    });

    // in the controllers methods of your choice:

    AuthManager::flowKey('teacher')->request($request)->register()->authenticate(); // 

    AuthManager::flowKey('student')->request($request)->register(); // student needs to authenticate again
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
